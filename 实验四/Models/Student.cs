﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace 实验四.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
    }
}
