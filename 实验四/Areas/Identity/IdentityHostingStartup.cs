﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using 实验四.Models;

[assembly: HostingStartup(typeof(实验四.Areas.Identity.IdentityHostingStartup))]
namespace 实验四.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup { public void Configure(IWebHostBuilder builder) { builder.ConfigureServices((context, services) => { services.AddDefaultIdentity<IdentityUser>().AddEntityFrameworkStores<MyDbContext>(); }); } }
}